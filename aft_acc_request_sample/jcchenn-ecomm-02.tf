# take note on name for the nested OU
module "jcchenn-ecomm02" {
  source = "./modules/aft-account-request"
  control_tower_parameters = {
    AccountEmail              = "jcchenn+ecomm-02@amazon.com"
    AccountName               = "jcchenn-ecomm-02"
    ManagedOrganizationalUnit = "Prod-Ecommerce (ou-snsi-szskzlf5)"
    SSOUserEmail              = "jcchenn@cjlabs.io"
    SSOUserFirstName          = "Jun"
    SSOUserLastName           = "Chen"
  }
  account_tags = {
    "Cost Center" = "AWS Control Service"
  }
  change_management_parameters = {
    change_requested_by = "jcchenn"
    change_reason       = "AWS Control Tower Account Factory for Terraform"
  }
  custom_fields = {
    group = "Prod-Ecommerce"
  }
  account_customizations_name = "ECOMM02"
}