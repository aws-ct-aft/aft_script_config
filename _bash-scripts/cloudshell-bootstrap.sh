#!/bin/bash

# install terraform
echo "Installing Terraform"
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

cp /usr/bin/terraform ~/bin

# install s3fs
sudo amazon-linux-extras install epel
sudo yum install s3fs-fuse

# git clone aft
git clone https://github.com/aws-ia/terraform-aws-control_tower_account_factory.git

