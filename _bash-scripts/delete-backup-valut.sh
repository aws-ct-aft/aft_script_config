#!/bin/bash

for ARN in $(aws backup list-recovery-points-by-backup-vault --backup-vault-name "$1" --query 'RecoveryPoints[].RecoveryPointArn' --output text); do 
	  echo "deleting ${ARN} ..."
	    aws backup delete-recovery-point --backup-vault-name "$1" --recovery-point-arn "${ARN}"
    done
