########################################################################################################
#  Version: v2.2 (contributor: jcchenn@)                                                               # 
#                                                                                                      # 
#  The aft-repo.sh is used to populate aft repostority. To run the script, issue the command           #
#      aft-repo.sh [create|clear] [REGION] [https|ssh]                                                 #
#                                                                                                      #
#       create|clone|clear:     use create to polpulate the aft remote repo                            #
#                               use clone to clone aft remote repo to local                            #
#                               use clear to clear local and aft remote repo                           #                
#       REGION:                 specify the control tower home region                                  #
#       https|ssh:              this is optional, by default it use https for git                      #
########################################################################################################

#!/bin/bash

usage()
{
  echo "Usage: aft-repo.sh [create|clone|clear] [REGION] [https|ssh]"
  exit 2
}

# argument and system variable
ARG3=${3:-https}

AFT_REPO="$(pwd)/aft-repo"
AFT_REPO_SAMPLE="$(pwd)/aft-repo-sample"
AFT_REPO_SAMPLE_URL="https://github.com/aws-samples/aft-workshop-sample"
mkdir -p $AFT_REPO
mkdir -p $AFT_REPO_SAMPLE

# exit if there is error, print out error code and line number
set -eo pipefail 
trap 'echo -e "\n ==> Command failed with exit code $? at line $LINENO ."' ERR

if  [[ -z $1 || -z $2 ]]; then
    usage
    exit 1
fi

# identify aft vcs provider from SSM prameter store.
AWS_REGION=$2
AFT_VCS=`aws ssm get-parameter --name /aft/config/vcs/provider --region $AWS_REGION | jq -r ".Parameter.Value"`

# define the mapping of codestart site url and vcs provider, the format is CodestarTypeMap_[vcs provider]="[provider site url]"
CodestarTypeMap_bitbucket="bitbucket.org" 
CodestarTypeMap_github="github.com"
if [ $AFT_VCS = "githubenterprise" ]; then
    CodestarTypeMap_githubenterprise=`aws ssm get-parameter --name /aft/config/vcs/github-enterprise-url --region $AWS_REGION | jq -r ".Parameter.Value"`
fi

# account customization variable
AFT_ACCOUNT_CUSTOMIZATIONS_REPO=`aws ssm get-parameter --name /aft/config/account-customizations/repo-name --region $AWS_REGION | jq -r ".Parameter.Value"`
AFT_ACCOUNT_CUSTOMIZATIONS_BRANCH=`aws ssm get-parameter --name /aft/config/account-customizations/repo-branch --region $AWS_REGION | jq -r ".Parameter.Value"`

# global customization variable
AFT_GLOBAL_CUSTOMIZATIONS_REPO=`aws ssm get-parameter --name /aft/config/global-customizations/repo-name --region $AWS_REGION | jq -r ".Parameter.Value"`
AFT_GLOBAL_CUSTOMIZATIONS_BRANCH=`aws ssm get-parameter --name /aft/config/global-customizations/repo-branch --region $AWS_REGION | jq -r ".Parameter.Value"`

# account provisioning customization variable
AFT_PROVISIONING_CUSTOMIZATIONS_REPO=`aws ssm get-parameter --name /aft/config/account-provisioning-customizations/repo-name --region $AWS_REGION | jq -r ".Parameter.Value"`
AFT_PROVISIONING_CUSTOMIZATIONS_BRANCH=`aws ssm get-parameter --name /aft/config/account-provisioning-customizations/repo-branch --region $AWS_REGION | jq -r ".Parameter.Value"`

# account request varialbe
AFT_ACCOUNT_REQUEST_REPO=`aws ssm get-parameter --name /aft/config/account-request/repo-name --region $AWS_REGION | jq -r ".Parameter.Value"`
AFT_ACCOUNT_REQUEST_BRANCH=`aws ssm get-parameter --name /aft/config/account-request/repo-branch --region $AWS_REGION | jq -r ".Parameter.Value"`

# constuct aft repo url
if [ $AFT_VCS = "codecommit" ]; then

    # build repo variable for codecommit vcs
    if [ $ARG3 = "ssh" ]; then
        # construct ssh repo url for codecommit
        AFT_ACCOUNT_CUSTOMIZATIONS_REPO_URL=`aws codecommit get-repository --repository-name $AFT_ACCOUNT_CUSTOMIZATIONS_REPO --region $AWS_REGION | jq -r ".repositoryMetadata.cloneUrlSsh"`
        AFT_GLOBAL_CUSTOMIZATIONS_REPO_URL=`aws codecommit get-repository --repository-name $AFT_GLOBAL_CUSTOMIZATIONS_REPO --region $AWS_REGION | jq -r ".repositoryMetadata.cloneUrlSsh"`
        AFT_PROVISIONING_CUSTOMIZATIONS_REPO_URL=`aws codecommit get-repository --repository-name $AFT_PROVISIONING_CUSTOMIZATIONS_REPO_REPO --region $AWS_REGION | jq -r ".repositoryMetadata.cloneUrlSsh"`
        AFT_ACCOUNT_REQUEST_REPO_URL=`aws codecommit get-repository --repository-name $AFT_ACCOUNT_REQUEST_REPO_REPO --region $AWS_REGION | jq -r ".repositoryMetadata.cloneUrlSsh"`
    else
        # construct https(grc) repo url for codecommit
        AFT_ACCOUNT_CUSTOMIZATIONS_REPO_URL="codecommit::$AWS_REGION://$AFT_ACCOUNT_CUSTOMIZATIONS_REPO"
        AFT_GLOBAL_CUSTOMIZATIONS_REPO_URL="codecommit::$AWS_REGION://$AFT_GLOBAL_CUSTOMIZATIONS_REPO"
        AFT_PROVISIONING_CUSTOMIZATIONS_REPO_URL="codecommit::$AWS_REGION://$AFT_PROVISIONING_CUSTOMIZATIONS_REPO"
        AFT_ACCOUNT_REQUEST_REPO_URL="codecommit::$AWS_REGION://$AFT_ACCOUNT_REQUEST_REPO"
    fi

else
    # build repo variable for non-codecommit vcs
    git_site_url="CodestarTypeMap_$AFT_VCS"

    user_AccountCustomization="$(cut -d'/' -f1 <<<"$AFT_ACCOUNT_CUSTOMIZATIONS_REPO")"
    user_GlobalCustomization="$(cut -d'/' -f1 <<<"$AFT_GLOBAL_CUSTOMIZATIONS_REPO")"
    user_ProvisioningCustomization="$(cut -d'/' -f1 <<<"$AFT_PROVISIONING_CUSTOMIZATIONS_REPO")"
    user_AccountRequest="$(cut -d'/' -f1 <<<"$AFT_ACCOUNT_REQUEST_REPO")"

    if [ $ARG3 = "ssh" ]; then
        AFT_ACCOUNT_CUSTOMIZATIONS_REPO_URL="git@${!git_site_url}:$AFT_ACCOUNT_CUSTOMIZATIONS_REPO.git"
        AFT_GLOBAL_CUSTOMIZATIONS_REPO_URL="git@${!git_site_url}:$AFT_GLOBAL_CUSTOMIZATIONS_REPO.git"
        AFT_PROVISIONING_CUSTOMIZATIONS_REPO_URL="git@${!git_site_url}:$AFT_PROVISIONING_CUSTOMIZATIONS_REPO.git"
        AFT_ACCOUNT_REQUEST_REPO_URL="git@${!git_site_url}:$AFT_ACCOUNT_REQUEST_REPO.git"
    else    
        AFT_ACCOUNT_CUSTOMIZATIONS_REPO_URL="https://$user_AccountCustomization@${!git_site_url}/$AFT_ACCOUNT_CUSTOMIZATIONS_REPO.git"
        AFT_GLOBAL_CUSTOMIZATIONS_REPO_URL="https://$user_GlobalCustomization@${!git_site_url}/$AFT_GLOBAL_CUSTOMIZATIONS_REPO.git"
        AFT_PROVISIONING_CUSTOMIZATIONS_REPO_URL="https://$user_GlobalCustomization@${!git_site_url}/$AFT_PROVISIONING_CUSTOMIZATIONS_REPO.git"
        AFT_ACCOUNT_REQUEST_REPO_URL="https://$user_GlobalCustomization@${!git_site_url}/$AFT_ACCOUNT_REQUEST_REPO.git"
    fi

    AFT_ACCOUNT_CUSTOMIZATIONS_REPO="$(cut -d'/' -f2 <<<"$AFT_ACCOUNT_CUSTOMIZATIONS_REPO")"
    AFT_GLOBAL_CUSTOMIZATIONS_REPO="$(cut -d'/' -f2 <<<"$AFT_GLOBAL_CUSTOMIZATIONS_REPO")"
    AFT_PROVISIONING_CUSTOMIZATIONS_REPO="$(cut -d'/' -f2 <<<"$AFT_PROVISIONING_CUSTOMIZATIONS_REPO")"
    AFT_ACCOUNT_REQUEST_REPO="$(cut -d'/' -f2 <<<"$AFT_ACCOUNT_REQUEST_REPO")"
fi

# populate aft repo
StringArray=("$AFT_ACCOUNT_CUSTOMIZATIONS_REPO;$AFT_ACCOUNT_CUSTOMIZATIONS_BRANCH;$AFT_ACCOUNT_CUSTOMIZATIONS_REPO_URL" \
            "$AFT_GLOBAL_CUSTOMIZATIONS_REPO;$AFT_GLOBAL_CUSTOMIZATIONS_BRANCH;$AFT_GLOBAL_CUSTOMIZATIONS_REPO_URL" \
            "$AFT_PROVISIONING_CUSTOMIZATIONS_REPO;$AFT_PROVISIONING_CUSTOMIZATIONS_BRANCH;$AFT_PROVISIONING_CUSTOMIZATIONS_REPO_URL" \
            "$AFT_ACCOUNT_REQUEST_REPO;$AFT_ACCOUNT_REQUEST_BRANCH;$AFT_ACCOUNT_REQUEST_REPO_URL" )

case $1 in
    "create")
        for str in ${StringArray[@]}; do
            arrIN=(${str//;/ })
   
            echo " =============== Creating ${arrIN[0]} repo =============== "
            
            cd $AFT_REPO_SAMPLE
            git clone --branch ${arrIN[0]} $AFT_REPO_SAMPLE_URL ${arrIN[0]}

            cd $AFT_REPO
            git clone ${arrIN[2]}
            
            cd $AFT_REPO/${arrIN[0]}
            cp -R $AFT_REPO_SAMPLE/${arrIN[0]}/*[^\.git] .
            git add -A
            git commit -m "Populate aft repo"

            BRANCH=$(git branch | sed 's/* //g')
            if [ $BRANCH = ${arrIN[1]} ]; then
                git push
            else
                git branch ${arrIN[1]}
                git checkout ${arrIN[1]}
                git push --set-upstream origin ${arrIN[1]}
            fi

        done;;
    
    "clone")
        for str in ${StringArray[@]}; do
            arrIN=(${str//;/ })
   
            echo " =============== Creating ${arrIN[0]} repo =============== "
            cd $AFT_REPO_SAMPLE
            git clone --branch ${arrIN[0]} $AFT_REPO_SAMPLE_URL ${arrIN[0]}

            cd $AFT_REPO
            git clone ${arrIN[2]}

        done;;

    "clear")
        for str in ${StringArray[@]}; do
            arrIN=(${str//;/ })
   
            echo " =============== Clearing ${arrIN[0]} repo =============== "
            cd $AFT_REPO/${arrIN[0]}
            rm -rf *
            git add -A
            git commit -m "clear"
            git push 

        done

        rm -rf $AFT_REPO_SAMPLE
        rm -rf $AFT_REPO
        ;;
    
    *)
        usage
        exit 1
        ;;
esac
